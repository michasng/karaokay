const OCTAVE = [
  'A', // 0
  'A#', // 1
  'B', // 2
  'C', // 3
  'C#', // 4
  'D', // 5
  'D#', // 6
  'E', // 7
  'F', // 8
  'F#', // 9
  'G', // 10
  'G#' // 11
];

let initDone;

let player;
let song;
let isPlaying = false;
let mics = [];
let lyricsViews, pitchViews;
let drawSeparator = false;

async function setup() {
  createCanvas(windowWidth, windowHeight);
  
  player = new YT.Player("player", {
    videoId: "i8dh9gDzmz8",
    playerVars: {
      controls: 0, // hide video controls
      disablekb: 1, // disable keyboard controls
      showinfo: 0, // hide title and uploader name
      rel: 0, // don't autoplay related videos
      iv_load_policy: 3, // don't show annotations
      fs: 0, // hide fullscreen button
      modestbranding: 1, // hide the youtube logo (not working)
    },
    events: {
      onReady: () => {
        print("player", player);
        createCanvas(player.a.offsetWidth, player.a.offsetHeight);
        print('setup');
      }
    }
  });

}

async function init() {

  await getAudioContext().resume();

  mics.push(new Microphone(0, 0, 0));
  // mics.push(new Microphone(0, 1, 0));
  for (mic of mics) await mic.init();

  song = new Song('Green Day', 'When I Come Around');
  const lyricsHeight = height / 8;
  lyricsViews = [];
  lyricsViews.push(new LyricsView(song, 0, 0, width, lyricsHeight));
  /*lyricsViews.push(
    new LyricsView(song, 0, height - lyricsHeight, width, lyricsHeight)
  );*/
  pitchViews = [];
  pitchViews.push(
    new PitchView(song, mics[0], 0, lyricsHeight, width, 3 * lyricsHeight, false)
  );
  /*
  pitchViews.push(
    new PitchView(song, mics[1], 0, height / 2, width, 3 * lyricsHeight, false)
  );*/
  await song.init();

  initDone = true;
  print('initDone');
}

async function mouseClicked() {
  if (!initDone) {
    await init();
  }

  // print('time', song.vid.time());
  if (isPlaying) song.pause();
  else song.play();
  isPlaying = !isPlaying;
}

function draw() {
  // background(0);
  clear();

  if (!initDone) return;

  // song.drawVideo();

  const w = width / mics.length;
  mics.forEach((mic, i) => mic.update(w * i + w / 2));

  // first draw the song, then all of it's views
  song.update();
  for (const lyricsView of lyricsViews) lyricsView.draw();
  for (const pitchView of pitchViews) {
    if (isPlaying) pitchView.recordPitch();
    pitchView.draw();
  }

  if (drawSeparator) {
    stroke(255);
    line(0, height / 2, width, height / 2);
    noStroke();
  }
}
