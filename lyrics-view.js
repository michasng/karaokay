class LyricsView {
  constructor(song, x = 0, y = height - height / 4, w = width, h = height / 4) {
    this.song = song;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.xCenter = x + w / 2;
    this.line1Y = y + h / 4;
    this.line2Y = y + (3 * h) / 4;

    // listen to line/tone changes
    this.song.onNextLine.push(() => this.nextLine());
    this.song.onNextTone.push(() => this.nextTone());
  }

  nextLine() {
    this.sectionBegin = 0;
    this.sectionWidth = 0;
    this.lineTextWidth = textWidth(this.song.currentLine.text);
    this.textStartX = this.x + (this.w - this.lineTextWidth) / 2;
  }

  nextTone() {
    const tone = this.song.currentTone;
    this.sectionBegin = this.sectionBegin + this.sectionWidth;
    if (tone.isPause) {
      if (this.song.isLastToneInLine())
        this.sectionWidth = width - this.sectionBegin;
      // to end of line
      else this.sectionWidth = 0; // no movement at all
    } else {
      // move to the end of the word
      this.sectionWidth = textWidth(tone.word);
    }
    this.beats = tone.beats;
  }

  draw() {
    const {
      currentLine,
      currentTone,
      noMoreLyrics,
      tonePercentage,
      lines,
      currentLineIndex
    } = this.song;

    if (noMoreLyrics) return;

    rectMode(CORNER);
    textSize(this.h / 3);
    textAlign(CENTER, CENTER);

    // draw semi-transparent background bar
    fill(0, 0, 0, 191);
    rect(this.x, this.y, this.w, this.h);

    // draw timing indicator and lyrics
    if (currentTone.isPause) {
      if (!this.song.isLastLine()) {
        fill(255);
        text(lines[currentLineIndex + 1].text, this.xCenter, this.line1Y);
        if (!this.song.isLastLine(1))
          text(lines[currentLineIndex + 2].text, this.xCenter, this.line2Y);
      }
    } else {
      fill(255, 0, 0);
      // inerpolate rect width within the tone's section by tonePercentage
      rect(
        this.textStartX,
        this.line1Y - textSize() / 2,
        this.sectionBegin + this.sectionWidth * tonePercentage,
        textSize()
      );
      fill(255);
      text(currentLine.text, this.xCenter, this.line1Y);
      // draw the next line if it exists
      if (!this.song.isLastLine())
        text(lines[currentLineIndex + 1].text, this.xCenter, this.line2Y);
    }
  }
}
