class Song {
  constructor(artist, title) {
    this.artist = artist;
    this.title = title;
    this.displayName = artist + " - " + title;

    this.onNextLine = [];
    this.onNextTone = [];
  }

  async init() {
    return new Promise((resolve, reject) => {
      loadJSON("assets/" + this.displayName + ".json", data => {
        this.lines = data.lines;
        this.bpm = data.bpm;

        // counting beats and combining text
        for (const line of this.lines) {
          line.beats = 0;
          line.text = "";
          for (const tone of line.tones) {
            line.beats += tone.beats;
            if (!tone.isPause) line.text += tone.word;
          }
        }
        this.reset();
        resolve();
      });
    });
  }

  reset() {
    this.accumulatedBeats = 0;
    this.noMoreLyrics = false;
    this.currentLineIndex = -1;
    this.currentToneIndex = -1;
    this.nextLine();
  }

  play() {
    player.playVideo();
  }

  pause() {
    player.pauseVideo();
  }

  /**
   * @param {number} offset any number != 0 will check for lines other than the currentLine
   */
  isLastLine(offset = 0) {
    return this.currentLineIndex + 1 + offset === this.lines.length;
  }

  isLastToneInLine() {
    return this.currentToneIndex + 1 === this.currentLine.tones.length;
  }

  nextLine() {
    this.currentLineIndex++;
    this.currentLine = this.lines[this.currentLineIndex];
    this.currentLine.startsAtBeat = this.accumulatedBeats;
    for (const cb of this.onNextLine) cb();
  }

  nextTone() {
    this.currentToneIndex++;
    this.currentTone = this.currentLine.tones[this.currentToneIndex];
    this.accumulatedBeats += this.currentTone.beats;

    for (const cb of this.onNextTone) cb();
  }

  drawVideo() {
    imageMode(CENTER);
    image(
      this.vid,
      width / 2,
      height / 2,
      this.vid.width * this.scale,
      this.vid.height * this.scale
    );
  }

  /**
   * @param {number} time the seconds to be converted to beats
   * @returns the time in beats, depending on the song's bpm.
   * Return value is floating point for precision.
   */
  timeToBeats(time) {
    return (time * this.bpm) / 60;
  }

  beatsToTime(beats) {
    return (beats / this.bpm) * 60;
  }

  update() {
    // ToDo: Check for time to end the song
    const time = player.getCurrentTime();

    if (this.noMoreLyrics) return;

    this.beat = this.timeToBeats(time);
    if (this.beat >= this.accumulatedBeats) {
      // next beat/line
      if (this.isLastToneInLine()) {
        if (this.isLastLine()) {
          this.noMoreLyrics = true;
          return;
        }

        this.nextLine();
        this.currentToneIndex = -1;
      }
      this.nextTone();
    }

    // update linePercentage
    this.beatInLine = this.beat - this.currentLine.startsAtBeat;
    this.linePercentage = this.beatInLine / this.currentLine.beats;

    // update tonePercentage
    const toneBeatsSoFar =
      this.beat - this.accumulatedBeats + this.currentTone.beats;
    this.tonePercentage = toneBeatsSoFar / this.currentTone.beats;
  }
}
