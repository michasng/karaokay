class PitchView {
  constructor(song, mic, x = 0, y = height / 8, w = width, h = height / 2, drawLines) {
    this.song = song;
    this.mic = mic;
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
    this.drawLines = drawLines;

    this.beatHeight = h / OCTAVE.length;
    // - beatHeight, bc beats are drawn in corner mode
    this.lastBeatY = y + h - this.beatHeight;

    // listen to line/tone changes
    this.song.onNextLine.push(() => this.nextLine());
    this.song.onNextTone.push(() => this.nextTone());
  }

  nextLine() {
    this.beatWidth = this.w / this.song.currentLine.beats;
    this.noteHistory = [];
  }

  nextTone() { }

  recordPitch() {
    if (this.mic.noteInOctave === NaN)
      return;
    if (this.song.currentTone.isPause)
      return;
    this.noteHistory.push(
      new NoteHistoryItem(
        this.mic.noteInOctave,
        this.song.beatInLine,
        this.mic.noteInOctave - this.song.currentTone.note
      )
    );
  }

  draw() {
    const {
      lines,
      currentLineIndex,
      noMoreLyrics,
      linePercentage,
      currentTone,
    } = this.song;

    if (noMoreLyrics) return;

    if (this.drawLines) {
      stroke(0);
      strokeWeight(1);
      for (let y = this.h; y > 0; y -= this.beatHeight)
        line(this.x, this.y + y, this.x + this.w, this.y + y);
    }

    rectMode(CORNER);
    ellipseMode(CORNER);

    fill(0, 0, 0, 127);
    stroke(255);
    let beatsSoFar = 0;
    for (const tone of lines[currentLineIndex].tones) {
      if (!tone.isPause) {
        rect(
          this.x + beatsSoFar * this.beatWidth,
          this.lastBeatY - this.beatHeight * tone.note,
          this.beatWidth * tone.beats,
          this.beatHeight,
          this.beatHeight / 2
        );
      }
      beatsSoFar += tone.beats;
    }
    noStroke();

    // draw note history
    this.noteHistory.forEach((item, index) => {
      if (item.noteOffset === 0) fill(0, 255, 0);
      else if (item.noteOffset === 1 || item.noteOffset === - 1) fill(0, 255, 0, 63);
      else fill(255, 0, 0, 63);
      const indicatorY = this.beatHeight * item.noteInOctave;
      circle(
        this.x + this.beatWidth * item.beatInLine,
        this.lastBeatY - indicatorY,
        this.beatHeight
      );
    });

    // draw indicator
    const { noteInOctave } = this.mic;
    if (currentTone.isPause) fill(0, 0, 255, 63);
    else if (noteInOctave === currentTone.note) fill(0, 255, 0);
    else fill(255, 0, 0);
    stroke(0);
    const indicatorY = this.beatHeight * noteInOctave;
    circle(
      this.x + linePercentage * this.w,
      this.lastBeatY - indicatorY,
      this.beatHeight
    );
    noStroke();
  }
}

class NoteHistoryItem {

  constructor(noteInOctave, beatInLine, noteOffset) {
    this.noteInOctave = noteInOctave;
    this.beatInLine = beatInLine;
    this.noteOffset = noteOffset;
  }

}
