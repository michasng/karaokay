class Microphone {

  constructor(index, channel, volume = 1, amplitudeThreshold = 0.02) {
    this.frequency = 0;
    this.channel = channel;
    this.volume = volume;
    this.amplitudeThreshold = amplitudeThreshold;

    this.sourceIndex = index + 2;
    this.name = 'mic ' + index;
    this.mic = new p5.AudioIn();
    print('constructor');
  }

  async init() {
    print('sources', await this.mic.getSources());
    this.mic.setSource(this.sourceIndex);
    this.mic.start(() => this.listening());
  }

  listening() {
    print(this.name, 'listening');
    // print(this.name, 'mic', this.mic);
    const ac = getAudioContext();
    const splitter = ac.createChannelSplitter(2);
    this.mic.output.connect(splitter);

    // connect to speakers
    const gainNode = ac.createGain();
    gainNode.gain.value = this.volume;
    splitter.connect(gainNode, this.channel, 0);
    gainNode.connect(ac.destination);
    // print(this.name, 'ac dest', ac.destination);

    const dest = ac.createMediaStreamDestination();
    // print(this.name, 'dest', dest);
    splitter.connect(dest, this.channel, 0);
    this.mic.stream = dest.stream;

    // used to measure the amplitude of it's input
    this.amplitude = new p5.Amplitude();
    splitter.connect(this.amplitude.input, this.channel, 0);

    this.pitch = ml5.pitchDetection(
      'crepe/',
      ac,
      dest.stream,
      () => {
        print(this.name, 'model loaded');
        this.getPitch();
      }
    );
    // print(this.name, 'pitch', this.pitch);

  }

  getPitch() {
    this.pitch.getPitch((err, freq) => this.gotPitch(err, freq));
  }

  gotPitch(err, freq) {
    // print(this.name, 'gotPitch');
    if (err) {
      console.error(err);
    } else {
      // this.mic.getLevel() only works for both channels
      if (freq !== null && this.amplitude.getLevel() > this.amplitudeThreshold) {
        // print(this.name, 'frequency', freq);
        // print(this.name, 'level', this.amplitude.getLevel());
        this.frequency = freq;
      }
    }
    this.getPitch();
  }

  update(x) {
    // formula from https://de.wikipedia.org/wiki/Frequenzen_der_gleichstufigen_Stimmung
    // using + 48 instead of 49 bc of 0-based array indexes
    this.note = Math.round(Math.log2(this.frequency / 440) * 12 + 48);
    this.noteInOctave = this.note % OCTAVE.length;
    this.letter = OCTAVE[this.noteInOctave] + Math.floor(this.note / OCTAVE.length);
    // print('this.note', this.noteInOctave, this.letter);

    if (false) {
      const h = height / 2;
      const w = 100;
      textAlign(CENTER, CENTER);
      fill(255);
      textSize(32);
      text(this.frequency.toFixed(2), x, h + h / 4);

      textSize(64);
      text(this.letter, x, h + h / 2);

      // vertical line in the center
      stroke(255);
      strokeWeight(4);
      line(x, 0, x, h);

      const y = (h * this.noteInOctave / OCTAVE.length);
      rectMode(CENTER);
      noStroke();
      fill(255, 0, 0);
      rect(x, h - y, w, w / 10);
    }
  }

}
